package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User add(@Nullable User user);

    @NotNull
    Collection<User> add(@NotNull Collection<User> users);

    @NotNull
    Collection<User> set(@NotNull Collection<User> users);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @Nullable
    User findOneByIndex(@Nullable Integer index);

    @NotNull
    User removeOne(@Nullable User user);

    @Nullable
    User removeOneById(@Nullable String id);

    @Nullable
    User removeOneByIndex(@Nullable Integer index);

    void removeAll();

    int getSize();

    boolean existsById(String id);

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeOneByLogin(@Nullable String login);

    @Nullable
    User removeOneByEmail(@Nullable String email);

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    User lockOneByLogin(@Nullable String login);

    @NotNull
    User unlockOneByLogin(@Nullable String login);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

}
