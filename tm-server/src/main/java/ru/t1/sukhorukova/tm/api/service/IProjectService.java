package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@Nullable Project project);

    @Nullable
    Project add(
            @Nullable String userId,
            @Nullable Project project
    );

    @NotNull
    Collection<Project> add(@NotNull Collection<Project> projects);

    @NotNull
    Collection<Project> set(@NotNull Collection<Project> projects);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @NotNull
    List<Project> findAll(
            @Nullable final String userId,
            @Nullable final ProjectSort sort
    );

    @Nullable
    Project findOneById(@Nullable String id);

    @Nullable
    Project findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Project findOneByIndex(@Nullable Integer index);

    @Nullable
    Project findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    Project removeOne(@Nullable Project project);

    Project removeOne(
            @Nullable String userId,
            @Nullable Project project
    );

    @Nullable
    Project removeOneById(@Nullable String id);

    @Nullable
    Project removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Project removeOneByIndex(@Nullable Integer index);

    @Nullable
    Project removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll();

    void removeAll(@Nullable String userId);

    int getSize();

    int getSize(@Nullable String userId);

    boolean existsById(String id);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    Project create(
            @Nullable final String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project updateById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project updateByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project changeProjectStatusById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull Status status
    );

    @NotNull
    Project changeProjectStatusByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull Status status
    );

}
