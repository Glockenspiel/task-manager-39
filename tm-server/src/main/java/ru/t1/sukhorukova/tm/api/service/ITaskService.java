package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@Nullable Task task);

    @Nullable
    Task add(
            @Nullable String userId,
            @Nullable Task task
    );

    @NotNull
    Collection<Task> add(@NotNull Collection<Task> tasks);

    @NotNull
    Collection<Task> set(@NotNull Collection<Task> tasks);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    List<Task> findAll(final String userId, final TaskSort sort);

    @Nullable
    Task findOneById(@Nullable String id);

    @Nullable
    Task findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Task findOneByIndex(@Nullable Integer index);

    @Nullable
    Task findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    Task removeOne(@Nullable Task task);

    Task removeOne(
            @Nullable String userId,
            @Nullable Task task
    );

    @Nullable
    Task removeOneById(@Nullable String id);

    @Nullable
    Task removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Task removeOneByIndex(@Nullable Integer index);

    @Nullable
    Task removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll();

    void removeAll(@Nullable String userId);

    int getSize();

    int getSize(@Nullable String userId);

    boolean existsById(String id);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    Task changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    void removeProjectById(
            @Nullable String userId,
            @Nullable String projectId
    );

}
