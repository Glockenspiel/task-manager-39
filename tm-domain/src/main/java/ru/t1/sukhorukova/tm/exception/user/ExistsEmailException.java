package ru.t1.sukhorukova.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}
