package ru.t1.sukhorukova.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.comparator.CreatedComparator;
import ru.t1.sukhorukova.tm.comparator.NameComparator;
import ru.t1.sukhorukova.tm.comparator.StatusComparator;
import ru.t1.sukhorukova.tm.model.Project;

import java.util.Comparator;

@Getter
public enum ProjectSort {

    BY_NAME("Sort by name", "NAME"),
    BY_STATUS("Sort by status", "STATUS"),
    BY_CREATED("Sort by created", "CREATED");

    @NotNull
    private final String name;

    @NotNull
    private final String columnName;

    ProjectSort(
            @NotNull final String name,
            @NotNull final String columnName
    ) {
        this.name = name;
        this.columnName = columnName;
    }

    @Nullable
    public static ProjectSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final ProjectSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
